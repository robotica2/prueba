boolean Pulsador;

void setup() {
  pinMode(3, INPUT);
  pinMode(13, OUTPUT);

  Pulsador = false;

}

void loop() {
  delayMicroseconds(100);

  Pulsador = digitalRead(3);
  if (Pulsador) {
    digitalWrite(13, HIGH);
    delay(3000);
    digitalWrite(13, LOW);
  }

}
